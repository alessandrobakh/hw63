<?php

namespace Tests\Feature;

use App\Models\Like;
use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UsersTest extends TestCase
{
    Use RefreshDatabase;
    /**
     * A basic feature test example.
     *@group users
     * @return void
     */
    public function test_redirect_main_login()
    {
        $response = $this->get('/');
        $response->assertRedirect('/login');
    }

    /**
     * A basic feature test example.
     *@group users
     * @return void
     */
    public function test_redirect_posts_login()
    {
        $response = $this->get('/posts/create');
        $response->assertRedirect('/login');
    }

    /**
     * A basic feature test example.
     *@group users
     * @return void
     */
    public function test_auth_main()
    {
        $user = User::factory()->has(Post::factory())->create();
        $this->actingAs($user);
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     *@group users
     * @return void
     */
    public function test_auth_post()
    {
        $user = User::factory()->has(Post::factory())->create();
        $this->actingAs($user);
        $response = $this->get("/posts/{$user->posts->first()->id}");
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     *@group users
     * @return void
     */
    public function test_redirect_logout()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $response = $this->post('logout');
        $response->assertRedirect('/');
    }

    /**
     * A basic feature test example.
     *@group users
     * @return void
     */
    public function test_like()
    {
        $user = User::factory()->has(Post::factory())->create();
        $like = Like::factory()->for($user)->for($user->posts->first())->create();
        $this->actingAs($user);
        $response = $this->post("/posts/{$user->posts->first()->id}/likes");
        $response->assertRedirect();
    }
}
