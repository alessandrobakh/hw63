<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View|Response
     */
    public function index()
    {
        $all_users = User::all();
        $user = Auth::user();
        $subs = $user->users;
        return view('users.index', compact('user', 'subs', 'all_users'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return Factory|View|Response
     */
    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }

    /**
     * @return int
     */
    static function followersCount()
    {
        $user = Auth::user();
        $count = 0;
        foreach ($user->users as $item){
            if ($item->follower_id = $user->id){
                $count += 1;
            }
        }
        return $count;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function followUser(Request $request)
    {
        $user = Auth::user();
        if (!$user->users()->where('follower_id', $user->id)->where('following_id', $request->input('user_id'))->exists()){
            $auth_user = Auth::user();
            $auth_user->users()->attach($request->input('user_id'));
        } else {
            $auth_user = Auth::user();
            $auth_user->users()->detach($request->input('user_id'));
        }
        return redirect()->back();
    }

    /**
     * @param $all_users
     * @return |null
     */
    static function randomUser($all_users)
    {
        $count = count($all_users);
        if ($count>0){
            $random_user = $all_users->find(rand(1, $count));
            return $random_user;
        }
        return NULL;
    }
}
