<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request, Post $post)
    {
        $comment = new Comment($request->all());
        $comment->user()->associate($request->user());
        $comment->post_id = $post->id;
        $comment->save();
        return redirect()->back();
    }
}
