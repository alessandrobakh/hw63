<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Models\Post;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View|void
     */
    public function index()
    {
        $user = Auth::user();
        $posts = $user->posts;
        return view('posts.index', compact('posts', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View|Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PostRequest $request
     * @return RedirectResponse|Response
     */
    public function store(PostRequest $request)
    {
        $post = new Post($request->all());
        $post->user()->associate($request->user());
        if ($request->hasFile('photo')) {
            $post->photo = $request->file('photo')->store('photos', 'public');
        }
        $post->save();
        return redirect()->route('posts.index')->with('success', 'Post created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post $post
     * @return Factory|View|Response
     */
    public function show(Post $post)
    {
        $user = Auth::user();
        return view('posts.show', compact('post', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return Factory|View|Response
     */
    public function edit(Post $post)
    {
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PostRequest $request
     * @param \App\Models\Post $post
     * @return RedirectResponse|Response
     */
    public function update(PostRequest $request, Post $post)
    {
        $data = $request->all();
        if ($request->hasFile('photo')){
            $path = $request->file('photo')->store('photos', 'public');
            $data['photo'] = $path;
        }
        $post->user()->associate($request->user());
        $post->update($data);
        return redirect()->route('posts.index')->with('success', 'Post updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return RedirectResponse|Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        return redirect()->route('posts.index')->with('success', 'Post deleted!');
    }
}
