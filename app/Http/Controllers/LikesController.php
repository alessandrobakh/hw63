<?php

namespace App\Http\Controllers;

use App\Models\Like;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request, Post $post)
    {
        $user = Auth::user();
        $like = Like::where('user_id', $user->id)->where('post_id', $post->id);
        if (! $like->exists()){
            $like = new Like();
            $like->user()->associate($request->user());
            $like->post_id = $request->input('post_id');
            $like->like = true;
            $like->save();
        } else {
            if ($like->get()->first()->like == true){
                $current_like = $like->get()->first();
                $current_like->like = false;
                $current_like->save();
            } else {
                $current_like = $like->get()->first();
                $current_like->like = true;
                $current_like->save();
            }
        }
        return redirect()->back();
    }
}
