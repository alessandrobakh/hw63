@extends('layouts.app')

@section('content')

    <h1>Your posts {{$user->name}}:</h1>


    @if($posts->count()>0)


        @foreach($posts as $post)
            <div class="card mb-3" style="max-width: 500px">
                <a href="{{route('posts.show', ['post' => $post])}}">
                    <img class="w-100" src="{{asset('/storage/' . $post->photo)}}" alt="{{asset('/storage/' . $post->photo)}}"></a>
                <div class="card-body">
                    <h5 class="card-title">{{$post->title}}</h5>
                    <p class="card-text">{{$post->description}}</p>
                    <p class="card-text">Likes: {{$post->likes->pluck('like')->sum()}}</p>
                </div>
                @if(\App\Models\Like::where('user_id', $user->id)->where('post_id', $post->id)->exists())
                    @if(\App\Models\Like::where('user_id', $user->id)->where('post_id', $post->id)->get()->first()->like == true)
                        <div class="ml-auto mr-4">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-suit-heart-fill" viewBox="0 0 16 16">
                                <path d="M4 1c2.21 0 4 1.755 4 3.92C8 2.755 9.79 1 12 1s4 1.755 4 3.92c0 3.263-3.234 4.414-7.608 9.608a.513.513 0 0 1-.784 0C3.234 9.334 0 8.183 0 4.92 0 2.755 1.79 1 4 1z"/>
                            </svg>
                        </div>
                    @endif
                @endif
                <div>
                    <form action="{{route('posts.likes.store', ['post' => $post])}}" method="post">
                        @csrf
                        <input type="hidden" name="post_id" value="{{$post->id}}">
                        <button class="btn btn-outline-info ml-2 mb-2" type="submit">Like</button>
                    </form>
                </div>
                <a class="btn btn-outline-warning col-3 mb-1" href="{{route('posts.edit', ['post' => $post])}}">Edit</a>
                <div>
                    <a class="btn btn-outline-info" href="{{route('posts.show', ['post' => $post])}}">Show comments</a>
                </div>
            </div>

        @endforeach


    @else

    <p>No posts</p>

    @endif

@endsection
