@extends('layouts.app')

@section('content')

    <form enctype="multipart/form-data" action="{{route('posts.update', ['post' => $post])}}" method="post">
        @csrf
        @method('put')

        <div class="row mb-3">
            <label for="title" class="col-form-label">Title</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="title" name="title" value="{{$post->title}}">
            </div>
            @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="row mb-3">
            <label for="description" class="col-form-label">Description</label>
            <div class="col-sm-6">
                <textarea class="form-control" name="description" id="description">{{$post->description}}</textarea>
            </div>
            @error('description')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <br>

        <img src="{{asset('/storage/' . $post->photo)}}" alt="{{$post->photo}}" style="width:100px;height:100px;"><br/><br>

        <div class="row mb-3">
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="photo" name="photo">
                <label class="custom-file-label" for="photo">Choose photo</label>
            </div>
            @error('photo')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <br>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection
