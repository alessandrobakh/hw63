@extends('layouts.app')

@section('content')

    <h1>{{$post->user->name}}</h1>

    <div class="card mb-3" style="max-width: 500px">
        <img src="{{asset('/storage/' . $post->photo)}}" alt="{{asset('/storage/' . $post->photo)}}">
        <div class="card-body">
            <h5 class="card-title">{{$post->title}}</h5>
            <p class="card-text">{{$post->description}}</p>
            <p class="card-text">Likes: {{$post->likes->pluck('like')->sum()}}</p>
        </div>
        @if(\App\Models\Like::where('user_id', $user->id)->where('post_id', $post->id)->exists())
            @if(\App\Models\Like::where('user_id', $user->id)->where('post_id', $post->id)->get()->first()->like == true)
                <div class="ml-auto mr-4">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-suit-heart-fill" viewBox="0 0 16 16">
                        <path d="M4 1c2.21 0 4 1.755 4 3.92C8 2.755 9.79 1 12 1s4 1.755 4 3.92c0 3.263-3.234 4.414-7.608 9.608a.513.513 0 0 1-.784 0C3.234 9.334 0 8.183 0 4.92 0 2.755 1.79 1 4 1z"/>
                    </svg>
                </div>
            @endif
        @endif
        <div>
            <form action="{{route('posts.likes.store', ['post' => $post])}}" method="post">
                @csrf
                <input type="hidden" name="post_id" value="{{$post->id}}">
                <button class="btn btn-outline-info ml-2 mb-2" type="submit">Like</button>
            </form>
        </div>
    </div>

    <h3>Comments: </h3>
    <br>

    <p>
        <a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Create comment</a>
    </p>
    <div class="row">
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <div class="card card-body">
                    <form action="{{route('posts.comments.store', ['post' => $post])}}" id="create-comment" method="post">
                        @csrf
                        <input type="hidden" id="post_id" value="{{$post->id}}">
                        <div class="form-group">
                            <label for="body">Comment</label>
                            <textarea name="body" class="form-control" id="body" rows="3" required></textarea>
                        </div>
                        <button id="create-comment-btn" type="submit" class="btn btn-outline-success">Add new
                            comment
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <br>

    <div class="all-comments">
        @foreach($post->comments as $comment)

            <div id="comment-{{$comment->id}}" class="border mb-1 p-1">
                <h5>{{$comment->user->name}}</h5>
                <p>{{$comment->body}}</p>
            </div>

        @endforeach
    </div>

@endsection
