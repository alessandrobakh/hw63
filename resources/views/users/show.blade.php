@extends('layouts.app')

@section('content')

    <h1>{{$user->name}}</h1>
    <form action="{{route('users.follow')}}" method="post">
        @csrf
        <input type="hidden" name="user_id" value="{{$user->id}}">
        <button class="btn btn-outline-primary" type="submit">Follow/Unfollow</button>
    </form>
    <br>
    <h5>Follows: {{$user->users->count()}}</h5>
    <h5>Followers: {{\App\Http\Controllers\UsersController::followersCount()}}</h5>

    @if ($user->id == \Illuminate\Support\Facades\Auth::user()->id)

        <a href="{{route('posts.index')}}">Edit my posts</a>

    @endif

    <br>

    <a href="{{route('posts.create')}}">Create new post</a>

    <br>
    <br>

    <h3>Posts:</h3>
    <div class="row row-cols-1 row-cols-md-3 g-4">
    @foreach($user->posts as $post)
            <a href="{{route('posts.show', ['post' => $post])}}">
                <img class="col mb-1" src="{{asset('/storage/' . $post->photo)}}" alt="{{asset('/storage/' . $post->photo)}}"></a>
    @endforeach
    </div>

@endsection
