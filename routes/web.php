<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\UsersController::class, 'index'])->middleware('auth');

Route::resource('users', \App\Http\Controllers\UsersController::class)->middleware('auth')->only(['show', 'index']);
Route::resource('posts', \App\Http\Controllers\PostsController::class)->middleware('auth');
Route::resource('posts.comments', \App\Http\Controllers\CommentsController::class)->middleware('auth')->only(['store']);
Route::resource('posts.likes', \App\Http\Controllers\LikesController::class)->middleware('auth')->only(['store']);

Route::post('users.follow', [\App\Http\Controllers\UsersController::class, 'followUser'])->middleware('auth')->name('users.follow');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
