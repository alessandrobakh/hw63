<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Like;
use App\Models\Post;
use App\Models\Sub;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
//        User::factory(5)->create();
        User::factory(6)
            ->has(User::factory(3))
            ->create();

        Post::factory(50)
            ->has(Comment::factory(15))
            ->create();
    }
}
